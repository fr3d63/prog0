import java.io.BufferedReader;
import java.io.InputStreamReader;

public class prog0 {

	public static String readLine() {
		String s = "0";
		try {
			InputStreamReader converter = new InputStreamReader(System.in);
			BufferedReader in = new BufferedReader(converter);
			s = in.readLine();
		} catch (Exception e) {
			System.out.println("Error! Exception: " + e);
		}
		return s;
	}


	public static void main(String[] args) {
		System.out.print("Geben Sie eine Zahl ein: ");
		Integer x = Integer.parseInt(readLine());

        System.out.print("Geben Sie noch eine Zahl ein: ");
		Integer y = Integer.parseInt(readLine());

		
		System.out.println("");
		System.out.println("Die Zahlen sind " + x + " und " + y + ".");
		
		System.out.println("Ergebnisse: ");
		System.out.println(x + " * " + y + " ist " + (x * y));
		System.out.println(x + " zum Quadrat ist " + (x * x));
		System.out.println(y + " zum Quadrat ist " + (y * y));
        System.out.println("mod (" + x + ") ist " + (x%2));
        System.out.println("mod (" + y + ") ist " + (y%2));
	}
}